package com.example.dognet.data;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.dognet.R;
import com.huawei.hms.ads.AdListener;
import com.huawei.hms.ads.AdParam;
import com.huawei.hms.ads.InterstitialAd;
import com.huawei.hms.ads.banner.BannerView;

public class BannerManager {
    //
    private AdParam adParm = null;
    private InterstitialAd interstitialAd = null;
    private AdListener adListener = null;

    //
    public BannerManager() {
        this.adParm = new AdParam.Builder().build();
    }

    public void initUpBanner(Activity activity) {
        BannerView upBannerView = activity.findViewById(R.id.hw_banner_view_up);
        upBannerView.loadAd(adParm);
        BannerView downBannerView = activity.findViewById(R.id.hw_banner_view_down);
        downBannerView.loadAd(adParm);
    }

    public void initInterstitialAd(Activity activity) {
        this.adListener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                showInterstitial(activity);
            }

            @Override
            public void onAdFailed(int errorCode) {

            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        };
    }

    public void loadInterstitialAd(Context context) {
        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdId("teste9ih9j0rc3");
        interstitialAd.setAdListener(adListener);

        AdParam adParam = new AdParam.Builder().build();
        interstitialAd.loadAd(adParam);
    }

    public void showInterstitial(Activity activity) {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show(activity);
        } else {
        }
    }

}
