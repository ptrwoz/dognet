package com.example.dognet.data;

public class OptionsData {
    private int recognitionType = -1;
    private int increase = -1;
    private int dogDetection = -1;
    private int detectionLevel = -1;
    private int raw = -1;

    public OptionsData(int recognitionType, int increase, int raw, int dogDetection, int detectionLevel) {
        this.recognitionType = recognitionType;
        this.increase = increase;
        this.raw = raw;
        this.dogDetection = dogDetection;
        this.detectionLevel = detectionLevel;
    }

    public void setRaw(int raw) {
        this.raw = raw;
    }

    public void setRecognitionType(int recognitionType) {
        this.recognitionType = recognitionType;
    }

    public void setIncrease(int increase) {
        this.increase = increase;
    }

    public int getRecognitionType() {
        return recognitionType;
    }

    public int getIncrease() {
        return this.increase;
    }

    public int getRaw() {
        return raw;
    }

    public int getDogDetection() {
        return dogDetection;
    }

    public void setDogDetection(int dogDetection) {
        this.dogDetection = dogDetection;
    }

    public int getDetectionLevel() {
        return detectionLevel;
    }

    public void setDetectionLevel(int detectionLevel) {
        this.detectionLevel = detectionLevel;
    }
}
