package com.example.dognet.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DogInfo implements Serializable {
    @SerializedName("id")
    public long id;
    public String breed;
    public int breedGroupFCI;
    public int fci_number;
    public String origin;
    public String height;
    public String weight;
    public String life;
    public String hairLoss1;
    public String exerciseNeed;
    public String icon;
    public String img;
    public String size;
    public String otherName;
    public int lifeInAnApartment;
    public int goodFirstDog;
    public int toleratesSolitude;
    public int toleratesCold;
    public int toleratesHot;
    public int affectionate;
    public int friendlyWithChildren;
    public int friendlyWithStrangers;
    public int friendlyWithOtherAnimals;
    public int hairLoss2;
    public int droolingLevel;
    public int easyCare;
    public int robustHealth;
    public int easyTrain;
    public int intelligent;
    public int tendencyBark;
    public int tendencyNibble;
    public int protective;
    public int instinctHunt;
    public int adventurousSpirit;
    public int energyLevel;
    public int levelOfIntensity;
    public int needForExercise;
    public int playful;
    public String[] colors;
    public String[] temperament;
    public String type;
    public String web;
    public DogInfo() {

    }
    public double[] getdataArray() {
        double[] array = new double[11];
        switch (this.size){
            case "Smallest":
                array[0] = 0;
                break;
            case "Small":
                array[0] = 0.7143;
                break;
            case "Small - Medium":
                array[0] = 1.4286;
                break;
            case "Medium":
                array[0] = 2.1429;
                break;
            case "Medium - Large":
                array[0] = 2.8571;
                break;
            case "Large":
                array[0] = 3.5714;
                break;
            case "Large - Giant":
                array[0] = 4.2857;
                break;
            case "Giant":
                array[0] = 5.0000;
                break;
        }

        array[1] = (double)this.intelligent;
        array[2] = (double)this.affectionate;
        array[3] = (double)this.goodFirstDog;
        array[4] = (double)this.easyTrain;
        array[5] = (double)this.friendlyWithOtherAnimals;
        array[6] = (double)this.energyLevel;
        array[7] = (double)this.tendencyBark;
        array[8] = (double)this.needForExercise;
        array[9] = (double)this.droolingLevel;
        return array;
    }
}
