package com.example.dognet.data;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedManager {
    private final String DATA_TAG = "DOGNET_DATA";
    private final SharedPreferences sharedPreferences;
    private Context context = null;

    public SharedManager(Context context) {
        this.context = context;
        this.sharedPreferences = this.context.getSharedPreferences(DATA_TAG, context.MODE_PRIVATE);
    }

    private void saveData(String key, String value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    private Object loadData(String key) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        return this.sharedPreferences.getString(key,"-1");
    }
    public void saveModelData(int value) {
        saveData("modelHeight", String.valueOf(value));
    }
    public int loadModelData() {
        int rt = Integer.parseInt((String) loadData("modelHeight"));
        return rt;
    }
    public void saveOptions(OptionsData optionsData) {
        saveData("recognitionType", "0");//String.valueOf(optionsData.getRecognitionType()));
        saveData("increase", String.valueOf(optionsData.getIncrease()));
        saveData("raw", String.valueOf(optionsData.getRaw()));
        saveData("dogdetection", String.valueOf(optionsData.getDogDetection()));
        saveData("detectionlevel", String.valueOf(optionsData.getDetectionLevel()));
    }
    public OptionsData loadOptions() {
        int rt = 0;//Integer.parseInt((String) loadData("recognitionType"));
        int i = Integer.parseInt((String) loadData("increase"));
        int r = Integer.parseInt((String) loadData("raw"));
        int d = Integer.parseInt((String) loadData("dogdetection"));
        int dl = Integer.parseInt((String) loadData("detectionlevel"));
        return new OptionsData(rt, i, r, d, dl);
    }
    public boolean isData() {
        int rt = Integer.parseInt((String) loadData("recognitionType"));
        int i = Integer.parseInt((String) loadData("increase"));
        int r = Integer.parseInt((String) loadData("raw"));
        int d = Integer.parseInt((String) loadData("dogdetection"));
        int dl = Integer.parseInt((String) loadData("detectionlevel"));
        if (i != -1 && r!=-1 && rt!= -1 && d!= - 1 && dl != -1)
            return true;
        else
            return false;
    }

}
