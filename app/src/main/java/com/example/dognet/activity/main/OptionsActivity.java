package com.example.dognet.activity.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dognet.R;
import com.example.dognet.data.OptionsData;
import com.example.dognet.data.SharedManager;

public class OptionsActivity extends AppCompatActivity {

    private final String TAG = "DogNet";
    //
    private Spinner spinner_detect_type = null;
    private TextView textView_detector_details = null;
    private TextView  textView_improve_detections_details = null;
    private TextView  textView_rawdata_details = null;
    private Switch switch_improve_detections = null;
    private SeekBar seekbar_level_detection = null;
    //private Switch switch_rawdata = null;
    private Switch switch_dog_detect = null;
    //
    private SharedManager sharedManager = null;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        InitOptionsActivity();
    }
    public void InitOptionsActivity() {
        OptionsData opt = null;
        this.sharedManager = new SharedManager(getApplicationContext());
        if(!this.sharedManager.isData()) {
            this.sharedManager.saveOptions(new OptionsData(0,0, 0, 0, 0));
            opt = new OptionsData(0,0,0, 0, 0);
        } else {
            opt = this.sharedManager.loadOptions();
        }
        /*this.textView_improve_detections_details = findViewById(R.*id.textView_improve_detections_details);
        //this.textView_rawdata_details = findViewById(R.id.textView_rawdata_details);
        this.textView_detector_details = findViewById(R.id.textView_detector_details);
        //this.textView_rawdata_details.setText(getResources().getString(R.string.rawText));
        this.textView_improve_detections_details.setText(getResources().getString(R.string.improveText));

        this.spinner_detect_type = findViewById(R.id.spinner_detect_type);*/
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.detector_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.seekbar_level_detection = findViewById(R.id.seekbar_level_detection);
        this.switch_dog_detect = findViewById(R.id.switch_dog_detect);
        if (switch_dog_detect.isChecked() == false) {
            seekbar_level_detection.setEnabled(false);
        } else
            seekbar_level_detection.setEnabled(true);
        seekbar_level_detection.setProgress(opt.getDetectionLevel());
        this.switch_dog_detect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveData();
            }
        });
        /*this.spinner_detect_type.setAdapter(adapter);
        this.spinner_detect_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] mTestArray = getResources().getStringArray(R.array.detector_details_array);
                textView_detector_details.setText(mTestArray[position]);
                saveData();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/
        /*this.switch_rawdata = findViewById(R.id.switch_rawdata);
        this.switch_rawdata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedManager.saveOptions(new OptionsData(spinner_detect_type.getSelectedItemPosition(),
                        switch_improve_detections.isChecked()==true?1:0, switch_rawdata.isChecked()==true?1:0));
            }
        });*/
        /*this.switch_improve_detections = findViewById(R.id.switch_improve_detections);
        this.switch_improve_detections.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveData();
            }
        });*/
        this.seekbar_level_detection.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                saveData();
            }
        });
        //this.spinner_detect_type.setSelection(opt.getRecognitionType());
        //this.switch_improve_detections.setChecked(opt.getIncrease()>0?true:false);
        this.switch_dog_detect.setChecked(opt.getDogDetection()>0?true:false);
        //this.switch_rawdata.setChecked(opt.getRaw()>0?true:false);
    }
    public void saveData() {
        //sharedManager.saveOptions(new OptionsData(spinner_detect_type.getSelectedItemPosition(),
        //        switch_improve_detections.isChecked()==true?1:0, 1, switch_dog_detect.isChecked()==true?1:0, seekbar_level_detection.getProgress()));
        sharedManager.saveOptions(new OptionsData(0,
                        0, 1, switch_dog_detect.isChecked()==true?1:0, seekbar_level_detection.getProgress()));
        if (switch_dog_detect.isChecked() == false) {
            seekbar_level_detection.setEnabled(false);
        } else {
            seekbar_level_detection.setEnabled(true);
        }
    }
    //
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.options:
                Log.d(TAG,"OptionsActivity change language");
                return true;*/
            case R.id.about:
                Log.d(TAG,"OptionsActivity about");
                return true;
            /*case R.id.help:
                Log.d(TAG,"OptionsActivity help");
                return true;*/
            case R.id.exit:
                Log.d(TAG,"OptionsActivity exit");
                this.finishAffinity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}