package com.example.dognet.activity.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SeekBar;

import com.example.dognet.R;
import com.example.dognet.data.DataManager;
import com.example.dognet.knn.KnnManager;
import com.example.dognet.layout.CustomDogList;
import com.example.dognet.layout.DogListView;

public class FinddogActivity extends AppCompatActivity {

    private DogListView dogListView = null;
    private ListView listViewDogs = null;
    //private CustomDogList customDogList = null;
    private DataManager dataManager = null;
    private KnnManager knnManager = null;
    int seekBarId[] = {
            R.id.textView_grid_part0,
            R.id.textView_grid_part1,
            R.id.textView_grid_part2,
            R.id.textView_grid_part3,
            R.id.textView_grid_part4,
            R.id.textView_grid_part5,
            R.id.textView_grid_part6,
            R.id.textView_grid_part7,
            R.id.textView_grid_part8,
            R.id.textView_grid_part9};
    private SeekBar[] seekBars = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finddog);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        this.initFinddogActivity();
    }
    private void initFinddogActivity() {
        this.dataManager = new DataManager();
        String jsonTxt = this.dataManager.loadJSONFromAsset(getApplicationContext());
        this.listViewDogs = (ListView) findViewById(R.id.listView_classificationImage);
        dogListView = new DogListView(this, this.listViewDogs, this.dataManager.jsonToList(jsonTxt));
        //dogListView.resetDogList();
        dogListView.refreshListViewDogs();
        this.knnManager = new KnnManager();
        this.knnManager.initDogData(dogListView.getDogs());

        seekBars = new SeekBar[seekBarId.length];
        for(int i=0; i<seekBarId.length; i++){
            this.seekBars[i] = findViewById(seekBarId[i]);
            this.seekBars[i].setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int[] predicterd = knnManager.predict(getData(),3);
                    dogListView.filterDogs(predicterd);
                    dogListView.refreshListViewDogs();
                }
            });
        }
        int[] predicterd = this.knnManager.predict(this.getData(),3);
        dogListView.filterDogs(predicterd);
        dogListView.refreshListViewDogs();
    }
    private double[] getData() {
        double[] data = new double[seekBarId.length];
        for(int i=0; i<data.length; i++) {
            data[i] = Double.valueOf(seekBars[i].getProgress());
        }
        return data;
    }

}