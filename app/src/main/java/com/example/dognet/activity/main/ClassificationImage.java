package com.example.dognet.activity.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dognet.R;
import com.example.dognet.data.DataManager;
import com.example.dognet.data.DogInfo;
import com.example.dognet.data.DogResult;
import com.example.dognet.data.OptionsData;
import com.example.dognet.data.SharedManager;
import com.example.dognet.layout.CustomDogList;
import com.example.dognet.layout.DogListView;
import com.example.dognet.model.HebingModel;
import com.example.dognet.model.InterpreterManager;
import com.example.dognet.model.LabelModel;
import com.example.dognet.model.LabelTfModel;
import com.example.dognet.model.ModelOperator;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.mlsdk.MLAnalyzerFactory;
import com.huawei.hms.mlsdk.classification.MLImageClassification;
import com.huawei.hms.mlsdk.classification.MLImageClassificationAnalyzer;
import com.huawei.hms.mlsdk.classification.MLLocalClassificationAnalyzerSetting;
import com.huawei.hms.mlsdk.common.MLApplication;
import com.huawei.hms.mlsdk.common.MLException;
import com.huawei.hms.mlsdk.common.MLFrame;
import com.huawei.hms.mlsdk.custom.MLModelOutputs;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ClassificationImage extends AppCompatActivity implements View.OnClickListener, InterpreterManager.ExceutorResult  {

    private final int MAX_HEIGHT = 600;
    private final String TAG = "DogNet";
    private static final String EXTRA_MESSAGE = "ClassificationImage";
    private final String IMG_FOLDER = "imgs/icons";
    private int img_count = 0;
    //
    private CardView cardView = null;
    private ListView listView_classificationImage = null;
    private TextView textView_classificationResult = null;
    private TextView textView_classificationResultValue = null;
    private ImageView imageView_classificationImage = null;
    private DataManager dataManager = null;
    //
    private CustomDogList customDogList = null;
    private InterpreterManager interpreterManager = null;
    //private InterpreterManager interpreterManager2  = null;
    private ModelOperator modelOperator = null;
    //private ModelOperator modelOperator2  = null;

    private OptionsData opt = null;
    private SharedManager sharedManager = null;
    private List<Bitmap> bitmaps = null;
    private List<DogInfo> dogs = null;
    //
    boolean isImageFitToScreen = false;
    private List<DogResult> dogResults = null;
    private List<MLImageClassification> dogRecognition = null;
    private String[][] dogResultsString = null;
    private final float[] dogLevelDetection = new float[]{0.3f, 0.6f, 0.8f};
    //
    private int imgNumber = -1;
    private DogListView dogListView = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classification_image);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        this.ClassificationImageInit();
    }

    void ClassificationImageInit() {
        Log.d(TAG,"ClassificationImage start");
        try {
            //this.textView_classificationResult = findViewById(R.id.textView_classificationResult);
            this.sharedManager = new SharedManager(getApplicationContext());
            if(!this.sharedManager.isData()) {
                this.sharedManager.saveOptions(new OptionsData(0,0, 0, 0, 0));
                this.opt = new OptionsData(0,0,0, 0, 0);
            } else {
                this.opt = this.sharedManager.loadOptions();
            }
            this.dataManager = new DataManager();
            this.dogResults = new ArrayList<>();
            this.dogRecognition = new ArrayList<>();
            this.listView_classificationImage = findViewById(R.id.listView_classificationImage);
            this.imageView_classificationImage = findViewById(R.id.imageView_classificationImage);
            this.imageView_classificationImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (img_count < bitmaps.size() - 1)
                        img_count ++;
                    else
                        img_count = 0;
                    imageView_classificationImage.setImageBitmap(bitmaps.get(img_count));
                }
            });
            this.textView_classificationResultValue = findViewById(R.id.textView_classificationResultValue);
            this.dataManager = new DataManager();
            String jsonTxt = this.dataManager.loadJSONFromAsset(getApplicationContext());
            this.listView_classificationImage = (ListView) findViewById(R.id.listView_classificationImage);
            this.dogListView = new DogListView(this, this.listView_classificationImage, this.dataManager.jsonToList(jsonTxt));

            Intent intent = getIntent();
            Bundle args = intent.getBundleExtra(EXTRA_MESSAGE);
            ArrayList<String> bitmapsUrl = (ArrayList<String>) args.getSerializable("ARRAYLIST");
            this.bitmaps = new ArrayList<Bitmap>();
            for(int i=0; i < bitmapsUrl.size(); i++) {
                Uri myUri = Uri.parse(bitmapsUrl.get(i));
                Bitmap b = MediaStore.Images.Media.getBitmap(this.getContentResolver(), myUri);

                bitmaps.add(Bitmap.createScaledBitmap(b, 299, 299, true));
            }
            this.imageView_classificationImage.setImageBitmap(this.bitmaps.get(img_count));
            this.modelOperator = new LabelModel(this, "model", "doglabels1.txt", 222);
            //this.modelOperator = new LabelModel(this, "ml_lable_model_hebing_device", "doglabelsDetection.txt", 91);

            MLApplication.initialize(getBaseContext());
            //this.interpreterManager2 = new InterpreterManager(modelOperator, this);
            this.interpreterManager = new InterpreterManager(modelOperator, this);
            //this.interpreterManager.close();
            //this.interpreterManager.changeModel(modelOperator2);

            this.imgNumber = bitmapsUrl.size();
            for(int i=0; i < bitmapsUrl.size(); i++) {
                localAnalyzer(this.bitmaps.get(i));
                this.interpreterManager.exec(this.bitmaps.get(i));
            }
            /*MyThread myThread = new MyThread();
            myThread.start();*/



            this.cardView = findViewById(R.id.card_view);
            this.fullScreenImage(true);
            /*this.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fullScreenImage(isImageFitToScreen);
                }
            });*/
            Log.d(TAG, "Image load success");
        } catch (IOException e) {
            Log.e(TAG, "Image load error " + e);
        }
    }
    private void fullScreenImage(boolean isImageFitToScreen) {
        if(isImageFitToScreen) {
            isImageFitToScreen=false;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            LinearLayout.LayoutParams layoutParams= (LinearLayout.LayoutParams) cardView.getLayoutParams();
            DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            layoutParams.width = width - 10;
            layoutParams.height = (int)(width - 10) * 4/5;

            cardView.setLayoutParams(layoutParams);
        }else{
            isImageFitToScreen=true;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            LinearLayout.LayoutParams layoutParams= (LinearLayout.LayoutParams) cardView.getLayoutParams();
            layoutParams.height = dpToPx(160);
            layoutParams.width = dpToPx(200);
            cardView.setLayoutParams(layoutParams);
        }
    }
    public int dpToPx(int dp) {
        float density = getApplication().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    @Override
    public void onClick(View v) {

    }
    private int[] findIdResult(String[][] stringArray) {
        int[] array = new int[stringArray.length];
        for (int i=0; i<stringArray.length; i++) {
            array[i] = 0;
            for (int j=0; j<this.dogListView.dogs.size(); j++) {
                if(stringArray[i][0].equals(this.dogListView.dogs.get(j).breed)) {
                    array[i] = j;
                    break;
                }
            }
        }
        return array;
    }
    private String[][] reduceResult(List<DogResult> dogResults) {
        List<DogResult> dogResults2 = new ArrayList<>();
        for (int i=0;i<dogRecognition.size(); i++) {
            if (this.opt.getDogDetection() == 1) {
                if (dogRecognition.get(i) != null)
                    if (dogRecognition.get(i).getPossibility() >= dogLevelDetection[this.opt.getDetectionLevel()] && dogRecognition.get(i).getName().matches("Dog") || this.opt.getDogDetection() == 0) {
                        dogResults2.add(dogResults.get(i));
                        //break;
                    } else if (dogResults.get(i).getResult() >= (20 * dogLevelDetection[this.opt.getDetectionLevel()]) && (dogRecognition.get(i).getName().matches("Animal") || dogRecognition.get(i).getName().matches("Pet")) || this.opt.getDogDetection() == 0) {
                        dogResults2.add(dogResults.get(i));
                        //break;
                    }
            } else {
                dogResults2.add(dogResults.get(i));
            }
        }
        dogResults = dogResults2;
        String[] array = new String[dogResults.size()];
        float[] fArray = new float[dogResults.size()];
        for (int i=0; i<dogResults.size(); i++) {
            array[i] = dogResults.get(i).getName();
            fArray[i] = dogResults.get(i).getResult();
        }
        Set<String> temp = new LinkedHashSet<String>( Arrays.asList(array));
        String[] result = temp.toArray( new String[temp.size()] );

        String[] array2 = new String[result.length];
        float[] fArray2 = new float[result.length];

        for (int i=0; i < result.length; i++) {
            float sum = 0;
            int no = 1;
            for (int j=0; j<dogResults.size(); j++) {
                if (result[i].equals(dogResults.get(j).getName())) {
                    sum += fArray[j];
                    no += 1;
                }
            }
            sum /= no;
            array2[i] = result[i];
            fArray2[i] = sum;
        };

        float[] fArray3 = fArray2.clone();
        int[] iArray3 = new int[result.length];
        for(int i=0; i<result.length; i++) {
            int a = getIndexOfLargest(fArray3);
            iArray3[i] = a;
            fArray3[a] = -1;
        }

        String[][] reduceResult = new String[temp.size()][2];

        for(int i=0; i<reduceResult.length; i++) {
            reduceResult[i][0] = array2[iArray3[i]];
            reduceResult[i][1] = String.valueOf(fArray2[iArray3[i]]);
        }

        return reduceResult;
    }
    public int getIndexOfLargest(float[] array)
    {
        if ( array == null || array.length == 0 ) return -1;

        int largest = 0;
        for ( int i = 1; i < array.length; i++ )
        {
            if ( array[i] > array[largest] ) largest = i;
        }
        return largest;
    }
    private MLImageClassificationAnalyzer analyzer = null;
    private void localAnalyzer(Bitmap bitmap) {
        //--------HERE TO CHANGE DIFFERENT MODE-------------
        MLLocalClassificationAnalyzerSetting deviceSetting =
                new MLLocalClassificationAnalyzerSetting.Factory().setMinAcceptablePossibility(0.8f).create();
        this.analyzer = MLAnalyzerFactory.getInstance().getLocalImageClassificationAnalyzer(deviceSetting);
        //----------------------------------------------------
        MLFrame frame = MLFrame.fromBitmap(bitmap);
        Task<List<MLImageClassification>> task = this.analyzer.asyncAnalyseFrame(frame);
        task.addOnSuccessListener(new OnSuccessListener<List<MLImageClassification>>() {
            @Override
            public void onSuccess(List<MLImageClassification> classifications) {
                boolean flag = true;
                for(int i=0; i<classifications.size(); i++) {
                    if ((classifications.get(i).getName().matches("Dog") || classifications.get(i).getName().matches("Animal") || classifications.get(i).getName().matches("Pet"))|| opt.getDogDetection() == 0) {
                        for(int j=0; j<3; j++)
                            dogRecognition.add(classifications.get(i));
                        flag = false;
                        break;

                    }
                }
                if (flag)
                    for(int j=0; j<3; j++)
                        dogRecognition.add(null);

                Log.e("","");
                //ClassificationImage.this.displaySuccess(classifications);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e("","");
                //ImageScanActivity.this.displayFailure();
            }
        });
    }
    @Override
    public boolean onResult(MLModelOutputs mlModelOutputs) throws MLException {
        String[][] dst = (String[][]) modelOperator.resultPostProcess(mlModelOutputs);
        for(int i=0; i<3; i++) {
            dogResults.add(new DogResult(dst[i][0], Float.parseFloat(dst[i][1].replace("%","").replace(",","."))));
        }
        this.imgNumber-=1;
        if (this.imgNumber == 0) {
            dogResultsString = this.reduceResult(dogResults);
            if (dogResultsString.length == 0)
                this.textView_classificationResultValue.setVisibility(View.VISIBLE);
            else {
                this.textView_classificationResultValue.setVisibility(View.GONE);
                int[] idx = findIdResult(dogResultsString);
                //if (this.opt.getRaw() == 1) {
                    //dogListView.resetDogList();
                    this.refreshResultsList(dogResultsString, idx);
                //}
            }
        } else {}
        return true;
    }
    private String[] postProcessingList(String[] array) {
        for(int i=0; i<array.length; i++) {
            String confi_text = getResources().getString(R.string.text_confidence);
            array[i] = confi_text + ": " + array[i] + "%";
        }
        return array;
    }
    private void refreshResultsList(String[][] dogResultsString, int[] idx) {

        String[] stringArray = (String[]) getColumn(dogResultsString, 0);
        String[] stringArray2 = (String[]) getColumn(dogResultsString, 1);
        stringArray2 = postProcessingList(stringArray2);

        int idx2[] = new int[idx.length];


        dogListView.resetDogList();
        dogListView.filterDogs(idx);
        dogListView.refreshResultsListViewDogs(stringArray2);
        //dogListView.refreshListViewDogs();
        /*this.customDogList = new CustomDogList(this, (String[]) stringArray, stringArray2, stringArray2, this.dogListView.getDogIcons());
        this.listView_classificationImage.setAdapter(this.customDogList);
        this.listView_classificationImage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(getBaseContext(), DogInfoActivity.class);
                Bundle args = new Bundle();
                String a = stringArray[position];
                args.putSerializable("ARRAYLIST",(Serializable)findDog(a));
                intent.putExtra(EXTRA_MESSAGE,args);
                startActivity(intent);
            }
        });*/
    }
    private DogInfo findDog(String dogName) {
        for(int i=0;i<this.dogs.size();i++) {
            if(dogName.equals(this.dogs.get(i).breed)) {
                return this.dogs.get(i);
            }
        }
        return null;
    }
    public static String[] getColumn(String[][] array, int index){
        String[] column = new String[array.length];
        for(int i=0; i<column.length; i++){
            column[i] = array[i][index];
        }
        return column;
    }
    public class MyThread extends Thread {

        public void run(){
            System.out.println("MyThread running");
            while (imgNumber != 1) {}
            interpreterManager.close();
            interpreterManager = new InterpreterManager(modelOperator, ClassificationImage.this);
            for(int i=0; i < bitmaps.size(); i++) {
                interpreterManager.exec(bitmaps.get(i));
            }

        }
    }
}