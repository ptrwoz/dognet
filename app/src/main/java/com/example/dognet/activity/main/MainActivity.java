package com.example.dognet.activity.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dognet.R;
import com.example.dognet.activity.menu.AboutActivity;
import com.example.dognet.data.BannerManager;
import com.example.dognet.data.DataManager;
import com.example.dognet.data.DogInfo;
import com.example.dognet.data.OptionsData;
import com.example.dognet.data.SharedManager;
import com.huawei.hms.ads.HwAds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity  extends AppCompatActivity implements View.OnClickListener {
    // max image number to analize
    private static final int IMG_MAX_NUMBER = 3;
    private static final String EXTRA_MESSAGE = "ClassificationImage";
    private static final int GALLERY_REQUEST = 255;
    private final String TAG = "DogNet";
    //
    private Button button_live_classification = null;
    private Button button_img_classification = null;
    private Button button_encyclopedia = null;
    private Button button_options = null;
    //
    private DataManager dataManager = null;
    private static List<DogInfo> dogs = null;
    private SharedManager sharedManager = null;

    public static List<DogInfo> getMainDogList() {
        return dogs;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.options:
                Log.d(TAG,"MainActivityInit options");
                intent = new Intent(this, OptionsActivity.class);
                startActivity(intent);
                return true;
            case R.id.about:
                Log.d(TAG,"MainActivityInit about");
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            /*case R.id.help:
                Log.d(TAG,"MainActivityInit help");
                return true;*/
            case R.id.exit:
                Log.d(TAG,"MainActivityInit exit");
                this.finishAffinity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void banerInit() {
        HwAds.init(this);
        BannerManager bannerManager = new BannerManager();
        bannerManager.initUpBanner(this);

        bannerManager.loadInterstitialAd(getApplicationContext());
        bannerManager.initInterstitialAd(this);
        bannerManager.showInterstitial(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);
        MainActivityInit();
    }
    void MainActivityInit() {
        Log.d(TAG,"MainActivityInit start");
        try {
            this.button_live_classification = (Button) findViewById(R.id.button_live_classification);
            this.button_img_classification = (Button) findViewById(R.id.button_img_classification);
            this.button_encyclopedia = (Button) findViewById(R.id.button_encyclopedia);
            this.button_options = (Button) findViewById(R.id.button_finddog);
            //
            this.button_live_classification.setOnClickListener(this);
            this.button_img_classification.setOnClickListener(this);
            this.button_encyclopedia.setOnClickListener(this);
            this.button_options.setOnClickListener(this);
            //
            this.sharedManager = new SharedManager(getApplicationContext());
            if(!this.sharedManager.isData()) {
                this.sharedManager.saveOptions(new OptionsData(0,0, 0, 0, 0));
            }
            //
            this.dataManager = new DataManager();
            String jsonTxt = this.dataManager.loadJSONFromAsset(getApplicationContext());
            this.dogs = this.dataManager.jsonToList(jsonTxt);
            //
            this.banerInit();
        } catch (Exception e) {
            Log.e(TAG,"MainActivityInit error = " + e);
            e.printStackTrace();
        }
        Log.d(TAG,"MainActivityInit success");
    }
    private void ClassificationImageStart(Intent data) {
        ArrayList<String> imageUris = new ArrayList<>();
        ClipData cd = data.getClipData();
        if ( cd == null ) {
            imageUris.add(data.getData().toString());
        } else {
            int count = data.getClipData().getItemCount();
            if (count > IMG_MAX_NUMBER) {
                Log.d(TAG, "Too much images");
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(getApplicationContext(), "Too much images (max. 3 images)", duration);
                toast.show();
                return;
            }
            else {
                for (int i = 0; i < count; i++) {
                    imageUris.add(data.getClipData().getItemAt(i).getUri().toString());
                }
            }
        }
        Log.d(TAG, "Images select success ");
        Intent intent = new Intent(this, ClassificationImage.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)imageUris);
        intent.putExtra(EXTRA_MESSAGE,args);
        startActivity(intent);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case GALLERY_REQUEST:
                    try {
                        ClassificationImageStart(data);
                    } catch (Exception e) {
                        Log.e(TAG, "Images select error " + e);
                    }
                    break;
            }
    }
    @Override
    public void onClick(View view) {
        Intent intent = null;
        int id = view.getId();
        switch (id) {
            case R.id.button_live_classification:
                Log.d(TAG,"live_classification button click");
                intent = new Intent(this, LiveClassification.class);
                startActivity(intent);
                break;
            case R.id.button_img_classification:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                photoPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoPickerIntent,"Select Pictures"), GALLERY_REQUEST);
                Log.d(TAG,"img_classification button click");
                break;
            case R.id.button_finddog:
                Log.d(TAG,"button_finddog: button click");
                intent = new Intent(this, FinddogActivity.class);
                startActivity(intent);
                break;
            case R.id.button_encyclopedia:
                Log.d(TAG,"button_encyclopedia button click");
                intent = new Intent(this, EncyklopediaActivity.class);
                startActivity(intent);
                break;
            default:
                Log.d(TAG,"button id no recognition!");
                break;
        }
    }
}