package com.example.dognet.activity.main;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import com.example.dognet.R;
import com.example.dognet.layout.DogListView;

import java.util.List;

public class EncyklopediaActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = "DogNet";
    //
    private SearchView searchView_search = null;
    private ListView listViewDogs = null;
    private Button button_search_breed = null;
    private Button button_search_fci = null;
    private Button button_search_othername = null;
    private Spinner spinner_type = null;
    private int filterArray[] = {1, 0, 0};
    //
    private DogListView dogListView = null;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encyklopedia);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        EncyklopediaActivityInit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.about:
                Log.d(TAG,"EncyklopediaActivity about");
                return true;
            case R.id.exit:
                Log.d(TAG,"EncyklopediaActivity exit");
                this.finishAffinity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void listViewDogsInit()
    {
        /*adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        this.searchView_search = (SearchView) findViewById(R.id.edit_text_search);
        searchView_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                dogListView.resetDogList();
                dogListView.filterDogs(searchView_search.getQuery().toString(), filterArray);
                dogListView.refreshListViewDogs();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dogListView.resetDogList();
                dogListView.filterDogs(searchView_search.getQuery().toString(), filterArray);
                dogListView.refreshListViewDogs();
                return false;
            }
        });
        searchView_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dogListView.resetDogList();
                dogListView.refreshListViewDogs();
            }
        });
        button_search_breed = findViewById(R.id.button_search_breed);
        button_search_breed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFilterChange(button_search_breed, 0);
                dogListView.resetDogList();
                dogListView.filterDogs(searchView_search.getQuery().toString(), filterArray);
                dogListView.refreshListViewDogs();
            }
        });
        button_search_fci = findViewById(R.id.button_search_fci);
        button_search_fci.setAlpha(.4f);
        button_search_fci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFilterChange(button_search_fci, 1);
                dogListView.resetDogList();
                dogListView.filterDogs(searchView_search.getQuery().toString(), filterArray);
                dogListView.refreshListViewDogs();
            }
        });
        button_search_othername = findViewById(R.id.button_search_othername);
        button_search_othername.setAlpha(.4f);
        button_search_othername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFilterChange(button_search_othername, 2);
                dogListView.resetDogList();
                dogListView.filterDogs(searchView_search.getQuery().toString(), filterArray);
                dogListView.refreshListViewDogs();
            }
        });
        dogListView.refreshListViewDogs();

    }
    private void buttonFilterChange(Button button, int i) {
        if (filterArray[i] == 0) {
            filterArray[i] = 1;
            button.setAlpha(1.0f);
        }
        else {
            filterArray[i] = 0;
            button.setAlpha(.4f);
        }
    }
    private void EncyklopediaActivityInit(){
        Log.d(TAG,"EncyklopediaActivity start");
        //HwAds.init(this);
        //BannerManager bannerManager = new BannerManager();
        //bannerManager.initUpBanner(this);
        try {
            /*this.spinner_type = findViewById(R.id.spinner_type);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.type, android.R.layout.simple_spinner_item);
            spinner_type.setAdapter(adapter);*/
            this.listViewDogs = (ListView) findViewById(R.id.viewListDogs);
            //
            dogListView = new DogListView(this, this.listViewDogs, MainActivity.getMainDogList());
            this.listViewDogsInit();

        } catch (Exception e){
            Log.e(TAG, "EncyklopediaActivityInit error " + e);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            default:
                Log.d(TAG,"button id no recognition!");
                break;
        }
    }
}