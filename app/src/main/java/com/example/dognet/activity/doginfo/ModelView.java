package com.example.dognet.activity.doginfo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.dognet.R;
import com.example.dognet.data.DataManager;
import com.example.dognet.data.DogInfo;
import com.example.dognet.data.SharedManager;
import com.huawei.hms.scene.math.Quaternion;
import com.huawei.hms.scene.math.Vector3;
import com.huawei.hms.scene.sdk.render.Animator;
import com.huawei.hms.scene.sdk.render.Camera;
import com.huawei.hms.scene.sdk.render.Light;
import com.huawei.hms.scene.sdk.render.Model;
import com.huawei.hms.scene.sdk.render.Node;
import com.huawei.hms.scene.sdk.render.RenderView;
import com.huawei.hms.scene.sdk.render.Renderable;
import com.huawei.hms.scene.sdk.render.Resource;
import com.huawei.hms.scene.sdk.render.Texture;
import com.huawei.hms.scene.sdk.render.Transform;

import java.lang.ref.WeakReference;
import java.util.List;

public class ModelView extends AppCompatActivity {

    private int maxHumanSize = 200;
    private int minHumanSize  = 120;
    private int humanSize = 180;
    private final String IMG_FOLDER_MAIN = "imgs/views";

    private DogInfo dogInfo = null;
    private static final String EXTRA_MESSAGE = "ClassificationImage";


    private static final class ModelLoadEventListener implements Resource.OnLoadEventListener<Model> {

        private final int humanHeight = 180;
        private final WeakReference<ModelView> weakRef;


        public ModelLoadEventListener(WeakReference<ModelView> weakRef) {
            this.weakRef = weakRef;
        }

        @Override
        public void onLoaded(Model model) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                Model.destroy(model);
                return;
            }

            sampleActivity.model = model;
            sampleActivity.modelNode = sampleActivity.renderView.getScene().createNodeFromModel(model);
            sampleActivity.modelNode.getComponent(Transform.descriptor())
                    .setPosition(new Vector3(0.f, 0.f, 0.f));
                    //.scale(new Vector3(0.02f, 0.02f, 0.02f));

            sampleActivity.modelNode.traverseDescendants(descendant -> {
                Renderable renderable = descendant.getComponent(Renderable.descriptor());
                if (renderable != null) {
                    renderable
                            .setCastShadow(true)
                            .setReceiveShadow(true);
                }
            });

            Animator animator = sampleActivity.modelNode.getComponent(Animator.descriptor());
            if (animator != null) {
                List<String> animations = animator.getAnimations();
                if (animations.isEmpty()) {
                    return;
                }
                animator
                        .setInverse(false)
                        .setRecycle(true)
                        .setSpeed(1.0f)
                        .play(animations.get(0));
            }
            String text = sampleActivity.dogInfo.height;
            String[] textArray = text.split(" - ");
            Integer value1 = Integer.valueOf(textArray[1]);
            Integer value2 = Integer.valueOf(textArray[1]);
            float factor =  (float) ((value1 + value2)/2) / humanHeight;
            sampleActivity.modelNode.getChildren().get(1).getComponent(Transform.descriptor()).translate(new Vector3(-5.0f, 0, 0));
            sampleActivity.modelNode.getChildren().get(0).getComponent(Transform.descriptor()).scale(new Vector3(factor, factor, factor));
            sampleActivity.modelNode.getChildren().get(1).getComponent(Transform.descriptor()).setScale(new Vector3(sampleActivity.p, sampleActivity.p, sampleActivity.p));
        }

        @Override
        public void onException(Exception e) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                return;
            }
            //Toast.makeText(sampleActivity, "failed to load model: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static final class SkyBoxTextureLoadEventListener implements Resource.OnLoadEventListener<Texture> {
        private final WeakReference<ModelView> weakRef;

        public SkyBoxTextureLoadEventListener(WeakReference<ModelView> weakRef) {
            this.weakRef = weakRef;
        }

        @Override
        public void onLoaded(Texture texture) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                Texture.destroy(texture);
                return;
            }

            sampleActivity.skyBoxTexture = texture;
            sampleActivity.renderView.getScene().setSkyBoxTexture(texture);
        }

        @Override
        public void onException(Exception e) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                return;
            }
            //Toast.makeText(sampleActivity, "failed to load texture: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static final class SpecularEnvTextureLoadEventListener implements Resource.OnLoadEventListener<Texture> {
        private final WeakReference<ModelView> weakRef;

        public SpecularEnvTextureLoadEventListener(WeakReference<ModelView> weakRef) {
            this.weakRef = weakRef;
        }

        @Override
        public void onLoaded(Texture texture) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                Texture.destroy(texture);
                return;
            }

            sampleActivity.specularEnvTexture = texture;
            sampleActivity.renderView.getScene().setSpecularEnvTexture(texture);
        }

        @Override
        public void onException(Exception e) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                return;
            }
            //Toast.makeText(sampleActivity, "failed to load texture: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static final class DiffuseEnvTextureLoadEventListener implements Resource.OnLoadEventListener<Texture> {
        private final WeakReference<ModelView> weakRef;

        public DiffuseEnvTextureLoadEventListener(WeakReference<ModelView> weakRef) {
            this.weakRef = weakRef;
        }

        @Override
        public void onLoaded(Texture texture) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                Texture.destroy(texture);
                return;
            }

            sampleActivity.diffuseEnvTexture = texture;
            sampleActivity.renderView.getScene().setDiffuseEnvTexture(texture);
        }

        @Override
        public void onException(Exception e) {
            ModelView sampleActivity = weakRef.get();
            if (sampleActivity == null || sampleActivity.destroyed) {
                return;
            }
            //Toast.makeText(sampleActivity, "failed to load texture: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean destroyed = false;

    private RenderView renderView;

    private Node cameraNode;
    private Node lightNode;

    private Model model;
    private Texture skyBoxTexture;
    private Texture specularEnvTexture;
    private Texture diffuseEnvTexture;
    private Node modelNode;
    private float p;
    private GestureDetector gestureDetector;
    private ScaleGestureDetector scaleGestureDetector;
    private DataManager dataManager = null;
    private TextView dogTextView = null;
    private TextView humanTextView = null;
    private SeekBar humanSeekBar = null;
    private SeekBar dogSeekBar = null;
    private TextView breed = null;
    private ImageView imageView_breedimg = null;
    private SharedManager sharedManager = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra(EXTRA_MESSAGE);
        this.dogInfo = (DogInfo) args.getSerializable("ARRAYMODEL");

        renderView = findViewById(R.id.render_view);
        this.dataManager = new DataManager();
        prepareScene();
        loadModel();
        loadTextures();
        addGestureEventListener();
        this.sharedManager = new SharedManager(getApplicationContext());
        initView();
    }

    private void initView() {
        this.imageView_breedimg = findViewById(R.id.imageView_breedimg);
        Bitmap img = this.dataManager.getBitmapFromAsset(getApplicationContext(), IMG_FOLDER_MAIN + "/" + this.dogInfo.img + ".jpg");
        if (img == null)
            img = this.dataManager.getBitmapFromAsset(getApplicationContext(), IMG_FOLDER_MAIN + "/" + "None_icon.jpg");
        this.imageView_breedimg.setImageBitmap(img);
        this.breed = findViewById(R.id.textView_breed);
        this.breed.setText(dogInfo.breed);
        this.humanTextView = findViewById(R.id.textView_grid_humanSize);
        this.humanSeekBar = findViewById(R.id.textView_grid_humanSize2);
        this.humanSeekBar.setMax(maxHumanSize);
        this.humanSeekBar.setMin(minHumanSize);
        this.humanSeekBar.setProgress(humanSize);
        humanTextView.setText(String.valueOf(humanSeekBar.getProgress()) + "cm");
        int a = this.sharedManager.loadModelData();
        if (a == -1) {
            a = this.maxHumanSize;
            this.sharedManager.saveModelData(a);

        }
        humanSeekBar.setProgress(a);
        humanTextView.setText(String.valueOf(humanSeekBar.getProgress()) + "cm");
        p = (float) humanSeekBar.getProgress() / maxHumanSize;


        this.humanSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                humanTextView.setText(String.valueOf(humanSeekBar.getProgress()) + "cm");
                float p = (float) progress / maxHumanSize;
                modelNode.getChildren().get(1).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
                sharedManager.saveModelData(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                humanTextView.setText(String.valueOf(humanSeekBar.getProgress()) + "cm");
                float p = (float) seekBar.getProgress() / maxHumanSize;
                modelNode.getChildren().get(1).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
                sharedManager.saveModelData(seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                humanTextView.setText(String.valueOf(humanSeekBar.getProgress()) + "cm");
                float p = (float) seekBar.getProgress() / maxHumanSize;
                modelNode.getChildren().get(1).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
                sharedManager.saveModelData(seekBar.getProgress());
            }
        });

        String text = dogInfo.height;
        String[] textArray = text.split(" - ");
        Integer value1 = Integer.valueOf(textArray[0]);
        Integer value2 = Integer.valueOf(textArray[1]);

        this.dogTextView = findViewById(R.id.textView_grid_dogSize);
        this.dogSeekBar = findViewById(R.id.textView_grid_dogSize2);

        this.dogSeekBar.setMax(Integer.valueOf(textArray[1]));
        this.dogSeekBar.setMin(Integer.valueOf(textArray[0]));
        this.dogSeekBar.setProgress((value1 + value2) / 2);
        this.dogSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                dogTextView.setText(String.valueOf(dogSeekBar.getProgress()) + "cm");
                float p = (float) seekBar.getProgress() / maxHumanSize;
                modelNode.getChildren().get(0).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                dogTextView.setText(String.valueOf(dogSeekBar.getProgress()) + "cm");
                float p = (float) seekBar.getProgress() / maxHumanSize;
                modelNode.getChildren().get(0).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                dogTextView.setText(String.valueOf(dogSeekBar.getProgress()) + "cm");
                float p = (float) seekBar.getProgress() / maxHumanSize;
                modelNode.getChildren().get(0).getComponent(Transform.descriptor()).setScale(new Vector3(p, p, p));
            }
        });
        dogTextView.setText(String.valueOf(dogSeekBar.getProgress()) + "cm");
    }

    @Override
    protected void onResume() {
        super.onResume();
        renderView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        renderView.pause();
    }

    @Override
    protected void onDestroy() {
        destroyed = true;
        renderView.destroy();
        super.onDestroy();
    }

    private void loadModel() {
        String dogModel = "";
        switch (this.dogInfo.breedGroupFCI) {
            /*case 1:
                dogModel = "SheepdogsAndCattledogs";
                break;
            case 2:
                dogModel = "PinscherAndSchnauzer";
                break;
            case 3:
                dogModel = "SheepdogsAndCattledogs";
                break;
            case 4:
                dogModel = "SheepdogsAndCattledogs";
                break;
            case 5:
                dogModel = "Pointing";
                break;
            case 6:
                dogModel = "Pointing";
                break;
            case 7:
                dogModel = "Pointing";
                break;
            case 8:
                dogModel = "Retrievers";
                break;
            case 9:
                dogModel = "Pointing";
                break;
            case 10:
                dogModel = "Sighthounds";
                break;*/
            default:
                dogModel = "scene3";
                break;
        }
        Model.builder()
                .setUri(Uri.parse("models/" + dogModel + ".gltf"))
                //.setUri(Uri.parse("models/scene3.gltf"))
                .load(this, new ModelLoadEventListener(new WeakReference<>(this)));
    }

    private void loadTextures() {
        Texture.builder()
                .setUri(Uri.parse("Forest/output_skybox.dds"))
                .load(this, new SkyBoxTextureLoadEventListener(new WeakReference<>(this)));
        Texture.builder()
                .setUri(Uri.parse("background/output_specular.dds"))
                .load(this, new SpecularEnvTextureLoadEventListener(new WeakReference<>(this)));
        Texture.builder()
                .setUri(Uri.parse("background/output_diffuse.dds"))
                .load(this, new DiffuseEnvTextureLoadEventListener(new WeakReference<>(this)));
    }

    private void prepareScene() {
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        cameraNode = renderView.getScene().createNode("mainCameraNode");
        cameraNode.addComponent(Camera.descriptor())
                .setProjectionMode(Camera.ProjectionMode.PERSPECTIVE)
                .setNearClipPlane(.1f)
                .setFarClipPlane(1000.f)
                .setFOV(60.f)
                .setAspect((float) displayMetrics.widthPixels / displayMetrics.heightPixels)
                .setActive(true);
        cameraNode.getComponent(Transform.descriptor())
                .setPosition(new Vector3(0.0f, 10.0f, 40.0f));

        lightNode = renderView.getScene().createNode("mainLightNode");
        lightNode.addComponent(Light.descriptor())
                .setType(Light.Type.POINT)
                .setColor(new Vector3(1.f, 1.f, 1.f))
                .setIntensity(1.f)
                .setCastShadow(false);
        lightNode.getComponent(Transform.descriptor())
                .setPosition(new Vector3(3.f, 3.f, 3.f));
    }

    private void addGestureEventListener() {
        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                if (modelNode != null) {
                    modelNode.getComponent(Transform.descriptor())
                            .rotate(new Quaternion(Vector3.UP, -0.001f * distanceX));
                }
                return true;
            }
        });
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                if (modelNode != null) {
                    /*float factor = detector.getScaleFactor();
                    modelNode.getComponent(Transform.descriptor())
                            .scale(new Vector3(factor, factor, factor));*/
                }
                return true;
            }
        });
        renderView.addOnTouchEventListener(motionEvent -> {
            boolean result = scaleGestureDetector.onTouchEvent(motionEvent);
            result = gestureDetector.onTouchEvent(motionEvent) || result;
            return result;
        });
    }
}
