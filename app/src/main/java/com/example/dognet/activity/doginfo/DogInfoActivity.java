package com.example.dognet.activity.doginfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dognet.R;
import com.example.dognet.activity.main.EncyklopediaActivity;
import com.example.dognet.data.DogInfo;
import com.example.dognet.layout.CustomDogList;
import com.example.dognet.layout.DogListView;
import com.huawei.hms.scene.common.base.error.exception.UpdateNeededException;
import com.huawei.hms.scene.sdk.render.SceneKit;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public class DogInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQ_CODE_UPDATE_SCENE_KIT = 10001;
    private boolean initialized = false;
    private static final String EXTRA_MESSAGE = "ClassificationImage";
    private final String TAG = "DogNet";
    private final String IMG_FOLDER_MAIN = "imgs/views";
    private final String IMG_FOLDER = "imgs/detailsicons";
    private DogInfo dogInfo;
    //
    //private TextView textView_details = null;
    private CustomDogList customDetailsList = null;
    private ListView listView_dog_details = null;
    private Button button_3DView = null;
    private TextView nameTextView = null;
    private ImageView imageView_breedimg = null;
    private GridLayout grid_details = null;
    private Button button_Wiki = null;
    private CardView cardView = null;
    private ImageView next_dog = null;
    private ImageView prev_dog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_info);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        DogInfoActivityInit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private boolean firstStart = true;
    boolean isImageFitToScreen = true;
    void DogInfoActivityInit() {
        Log.d(TAG,"DogInfoActivity start");
        try {
            if (firstStart) {
                Intent intent = getIntent();
                Bundle args = intent.getBundleExtra(EXTRA_MESSAGE);
                this.dogInfo = (DogInfo) args.getSerializable("ARRAYLIST");
            }
            this.next_dog = findViewById(R.id.next_dog);
            this.next_dog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dogInfo.id < DogListView.dogs.size()) {
                        firstStart = false;
                        dogInfo = DogListView.dogs.get((int) dogInfo.id + 1 - 1);
                        DogInfoActivityInit();
                    }
                }
            });
            this.prev_dog = findViewById(R.id.prev_dog);
            this.prev_dog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dogInfo.id - 1 > 0) {
                        firstStart = false;
                        dogInfo = DogListView.dogs.get((int) dogInfo.id - 1 - 1);
                        DogInfoActivityInit();
                    }
                }
            });
            this.nameTextView = findViewById(R.id.textView_breed);
            this.nameTextView.setText(this.dogInfo.breed);
            this.imageView_breedimg = findViewById(R.id.imageView_breedimg);

            this.cardView = findViewById(R.id.card_view);
            this.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isImageFitToScreen) {
                        isImageFitToScreen=false;
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        LinearLayout.LayoutParams layoutParams= (LinearLayout.LayoutParams) cardView.getLayoutParams();
                        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
                        int width = metrics.widthPixels;
                        int height = metrics.heightPixels;
                        layoutParams.width = width - 10;
                        layoutParams.height = (int)(width - 10) * 4/5;

                        cardView.setLayoutParams(layoutParams);
                    }else{
                        isImageFitToScreen=true;
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        LinearLayout.LayoutParams layoutParams= (LinearLayout.LayoutParams) cardView.getLayoutParams();
                        layoutParams.height = dpToPx(160);
                        layoutParams.width = dpToPx(200);
                        cardView.setLayoutParams(layoutParams);
                    }
                }
            });
            this.button_Wiki = findViewById(R.id.button_Wiki);
            this.button_Wiki.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(dogInfo.web != null)
                    {
                        if (dogInfo.web.length() > 0) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dogInfo.web));
                            startActivity(browserIntent);
                        }
                    }
                }
            });
            Bitmap img = this.getBitmapFromAsset(getApplicationContext(), IMG_FOLDER_MAIN + "/" + this.dogInfo.img + ".jpg");
            if (img == null) {
                img = this.getBitmapFromAsset(getApplicationContext(), IMG_FOLDER_MAIN + "/" + this.dogInfo.img + ".JPG");
                if (img == null) {
                    img = this.getBitmapFromAsset(getApplicationContext(), IMG_FOLDER_MAIN + "/" + "None_icon.jpg");
                }
            }
            this.imageView_breedimg.setImageBitmap(img);

            this.button_3DView = findViewById(R.id.button_3DView);
            this.button_3DView.setOnClickListener(this);

            this.grid_details = findViewById(R.id.groupId);
            this.initDetailsList();

            Log.d(TAG, "Dog data load success");
        } catch (Exception e) {
            Log.e(TAG, "Dog data load error " + e);
        }
    }
    public int dpToPx(int dp) {
        float density = getApplication().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    private String stringArray2String(String[] arr)
    {
        String stringText = "";
        for(int i=0; i<arr.length; i++) {
            if (i < arr.length - 1)
                stringText = stringText + arr[i] + ", ";
            else
                stringText = stringText + arr[i];
        }
        return stringText;
    }

    private void initDetailsList()
    {
        TextView textView_grid_breed = findViewById(R.id.textView_grid_breed);
        textView_grid_breed.setText(this.dogInfo.breed);
        TextView otherName = findViewById(R.id.textView_grid_otherName);
        otherName.setText(this.dogInfo.otherName);
        //TextView type = findViewById(R.id.textView_grid_fci_type);
        //type.setText(this.dogInfo.type);
        TextView colors = findViewById(R.id.textView_grid_colors);
        colors.setText(stringArray2String(this.dogInfo.colors));
        TextView temperament = findViewById(R.id.textView_grid_temperament);
        temperament.setText(stringArray2String(this.dogInfo.temperament));
        TextView size = findViewById(R.id.textView_grid_size);
        size.setText(this.dogInfo.size);
        TextView fci_number = findViewById(R.id.textView_grid_fci_number);
        fci_number.setText(String.valueOf(this.dogInfo.fci_number));
        TextView fci_group = findViewById(R.id.textView_grid_fci_group);
        String[] groupArray = getResources().getStringArray(R.array.fcigroup);
        fci_group.setText(String.valueOf(groupArray[this.dogInfo.breedGroupFCI - 1]) + " - " + String.valueOf(this.dogInfo.breedGroupFCI));
        TextView origin = findViewById(R.id.textView_grid_origin);
        origin.setText(String.valueOf(this.dogInfo.origin));
        TextView height = findViewById(R.id.textView_grid_height);
        if (this.dogInfo.height != null)
            if (this.dogInfo.height.length() > 0)
                height.setText(String.valueOf(this.dogInfo.height) + " cm");
            else
                height.setText("");
        else
            height.setText("");
        TextView weight = findViewById(R.id.textView_grid_weight);
        if (this.dogInfo.weight != null)
            if (this.dogInfo.weight.length() > 0)
                weight.setText(String.valueOf(this.dogInfo.weight) + " kg");
            else
                weight.setText("");
        else
            weight.setText("");
        TextView life = findViewById(R.id.textView_grid_life);
        if (this.dogInfo.life != null)
            if (this.dogInfo.life.length() > 0)
                life.setText(String.valueOf(this.dogInfo.life) + " years");
            else
                life.setText("");
        else
            life.setText("");
        //TextView hairLoss1 = findViewById(R.id.textView_grid_hairLoss1);
        //hairLoss1.setText(String.valueOf(this.dogInfo.hairLoss1));
        TextView exerciseNeed = findViewById(R.id.textView_grid_exerciseNeed);
        exerciseNeed.setText(String.valueOf(this.dogInfo.exerciseNeed));
        if (this.dogInfo.lifeInAnApartment != -1) {
            RatingBar lifeInAnApartment = findViewById(R.id.ratingBar_grid_lifeInAnApartment);
            lifeInAnApartment.setProgress(this.dogInfo.lifeInAnApartment);
            RatingBar ratingBar_grid_goodFirstDog = findViewById(R.id.ratingBar_grid_goodFirstDog);
            ratingBar_grid_goodFirstDog.setProgress(this.dogInfo.goodFirstDog);
            RatingBar ratingBar_grid_leratesSolitude = findViewById(R.id.ratingBar_grid_leratesSolitude);
            ratingBar_grid_leratesSolitude.setProgress(this.dogInfo.toleratesSolitude);
            RatingBar ratingBar_grid_leratesCold = findViewById(R.id.ratingBar_grid_leratesCold);
            ratingBar_grid_leratesCold.setProgress(this.dogInfo.toleratesCold);
            RatingBar ratingBar_grid_leratesHot = findViewById(R.id.ratingBar_grid_leratesHot);
            ratingBar_grid_leratesHot.setProgress(this.dogInfo.toleratesHot);
            RatingBar ratingBar_grid_affectionate = findViewById(R.id.ratingBar_grid_affectionate);
            ratingBar_grid_affectionate.setProgress(this.dogInfo.affectionate);
            RatingBar ratingBar_grid_friendlyWithChildren = findViewById(R.id.ratingBar_grid_friendlyWithChildren);
            ratingBar_grid_friendlyWithChildren.setProgress(this.dogInfo.friendlyWithChildren);
            RatingBar ratingBar_grid_friendlyWithStrangers = findViewById(R.id.ratingBar_grid_friendlyWithStrangers);
            ratingBar_grid_friendlyWithStrangers.setProgress(this.dogInfo.friendlyWithStrangers);
            RatingBar ratingBar_grid_friendlyWithOtherAnimals = findViewById(R.id.ratingBar_grid_friendlyWithOtherAnimals);
            ratingBar_grid_friendlyWithOtherAnimals.setProgress(this.dogInfo.friendlyWithOtherAnimals);
            RatingBar ratingBar_grid_hairLoss2 = findViewById(R.id.ratingBar_grid_hairLoss2);
            ratingBar_grid_hairLoss2.setProgress(this.dogInfo.hairLoss2);
            RatingBar ratingBar_grid_droolingLevel = findViewById(R.id.ratingBar_grid_droolingLevel);
            ratingBar_grid_droolingLevel.setProgress(this.dogInfo.droolingLevel);
            RatingBar ratingBar_grid_easyCare = findViewById(R.id.ratingBar_grid_easyCare);
            ratingBar_grid_easyCare.setProgress(this.dogInfo.easyCare);
            RatingBar ratingBar_grid_robustHealth = findViewById(R.id.ratingBar_grid_robustHealth);
            ratingBar_grid_robustHealth.setProgress(this.dogInfo.robustHealth);
            RatingBar ratingBar_grid_easyTrain = findViewById(R.id.ratingBar_grid_easyTrain);
            ratingBar_grid_easyTrain.setProgress(this.dogInfo.easyTrain);
            RatingBar ratingBar_grid_intelligent = findViewById(R.id.ratingBar_grid_intelligent);
            ratingBar_grid_intelligent.setProgress(this.dogInfo.intelligent);
            RatingBar ratingBar_grid_tendencyBark = findViewById(R.id.ratingBar_grid_tendencyBark);
            ratingBar_grid_tendencyBark.setProgress(this.dogInfo.tendencyBark);
            RatingBar ratingBar_grid_tendencyNibble = findViewById(R.id.ratingBar_grid_tendencyNibble);
            ratingBar_grid_tendencyNibble.setProgress(this.dogInfo.tendencyNibble);
            RatingBar ratingBar_grid_protective = findViewById(R.id.ratingBar_grid_protective);
            ratingBar_grid_protective.setProgress(this.dogInfo.protective);
            RatingBar ratingBar_grid_instinctHunt = findViewById(R.id.ratingBar_grid_instinctHunt);
            ratingBar_grid_instinctHunt.setProgress(this.dogInfo.instinctHunt);
            RatingBar ratingBar_grid_adventurousSpirit = findViewById(R.id.ratingBar_grid_adventurousSpirit);
            ratingBar_grid_adventurousSpirit.setProgress(this.dogInfo.adventurousSpirit);
            RatingBar ratingBar_grid_energyLevel = findViewById(R.id.ratingBar_grid_energyLevel);
            ratingBar_grid_energyLevel.setProgress(this.dogInfo.energyLevel);
            RatingBar ratingBar_grid_levelOfIntensity = findViewById(R.id.ratingBar_grid_levelOfIntensity);
            ratingBar_grid_levelOfIntensity.setProgress(this.dogInfo.levelOfIntensity);
            RatingBar ratingBar_grid_needForExercise = findViewById(R.id.ratingBar_grid_needForExercise);
            ratingBar_grid_needForExercise.setProgress(this.dogInfo.needForExercise);
            RatingBar ratingBar_grid_playful = findViewById(R.id.ratingBar_grid_playful);
            ratingBar_grid_playful.setProgress(this.dogInfo.playful);
            this.showDetails(true);

        } else {
            this.showDetails(false);
        }
    }
    private void showDetails(boolean show) {
        for(int index = 56; index < this.grid_details.getChildCount() - 4; index++) {
            View nextChild = this.grid_details.getChildAt(index);
            if(show)
                nextChild.setVisibility(View.VISIBLE);
            else
                nextChild.setVisibility(View.GONE);
        }
    }
    public Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.button_3DView:
                Log.d(TAG, "3d model view button click");
                if (dogInfo.height.length() > 0) {
                    if (!initialized) {
                        initializeSceneKit();
                        return;
                    }
                } else {

                }
                break;
            default:
                Log.d(TAG, "button id no recognition!");
                break;
        }
    }
    private void initializeSceneKit() {
        if (initialized) {
            return;
        }
        SceneKit.Property property = SceneKit.Property.builder()
                .setAppId("${app_id}")
                .setGraphicsBackend(SceneKit.Property.GraphicsBackend.GLES)
                .build();
        try {
            SceneKit.getInstance()
                    .setProperty(property)
                    .initializeSync(getApplicationContext());
            initialized = true;
            //Toast.makeText(this, "SceneKit initialized", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), ModelView.class);
            Bundle args = new Bundle();
            args.putSerializable("ARRAYMODEL", (Serializable) dogInfo);
            intent.putExtra(EXTRA_MESSAGE, args);
            startActivity(intent);
            initialized = false;
        } catch (UpdateNeededException e) {
            startActivityForResult(e.getIntent(), REQ_CODE_UPDATE_SCENE_KIT);
        } catch (Exception e) {
            Toast.makeText(this, "failed to initialize SceneKit: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}