package com.example.dognet.knn;

import com.example.dognet.data.DogInfo;

import java.util.ArrayList;
import java.util.List;

public class KnnManager {

    List<double[]> knnArray = null;
    public void initDogData(List<DogInfo> dogs) {
        this.knnArray = new ArrayList<>();
        for(int i=0; i<dogs.size(); i++)
        {
            knnArray.add(dogs.get(i).getdataArray());
        }
    }
    public int[] predict(double[] data, int n) {
        if (this.knnArray != null) {
            int minIndex[] = new int[n];
            double dist[] = new double[this.knnArray.size()];
            for(int i=0; i<this.knnArray.size(); i++) {
                dist[i] = Math.abs(cosineSimilarity(data, this.knnArray.get(i)));
            }
            for(int i=0; i<n; i++) {
                minIndex[i] = indexOfMax(dist);
                dist[minIndex[i]] = -100;
            }
            return minIndex;
        } else
            return null;
    }
    private int indexOfMax(double[] arr) {
        int imax = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[imax]) {
                imax = i;
            }
        }
        return imax;
    }
    public double cosineSimilarity(double[] vectorA, double[] vectorB) {
        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < vectorA.length; i++) {
            if (vectorA[i] != 0) {
                dotProduct += vectorA[i] * vectorB[i];
                normA += Math.pow(vectorA[i], 2);
                normB += Math.pow(vectorB[i], 2);
            }
        }
        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }

}
