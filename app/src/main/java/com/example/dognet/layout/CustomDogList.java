package com.example.dognet.layout;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dognet.R;

import java.util.ArrayList;

public class CustomDogList extends ArrayAdapter {
    private String[] dogsNames;
    private String[] dogDetails;
    private String[] dogFci;
    private Bitmap[] iconBitmaps;
    private Activity context;

    public CustomDogList(Activity context, String[] dogsNames, String[] dogDetails, String[] dogfcis, Bitmap[] iconBitmaps) {
        super(context, R.layout.item_img, dogsNames);
        this.context = context;
        this.dogsNames = dogsNames;
        this.dogDetails = dogDetails;
        this.dogFci = dogfcis;
        this.iconBitmaps = iconBitmaps;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        if(convertView==null)
            row = inflater.inflate(R.layout.item_img, null, true);
        TextView textViewCountry = (TextView) row.findViewById(R.id.textViewCountry);
        TextView textViewCapital = (TextView) row.findViewById(R.id.textViewCapital);
        TextView textViewFci = (TextView) row.findViewById(R.id.textViewFci);
        ImageView imageFlag = (ImageView) row.findViewById(R.id.imageViewFlag);

        textViewCountry.setText(dogsNames[position]);
        textViewCapital.setText(dogDetails[position]);
        textViewFci.setText(dogFci[position]);
        imageFlag.setImageBitmap(iconBitmaps[position]);

        return  row;
    }
}
