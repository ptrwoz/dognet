package com.example.dognet.layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dognet.activity.doginfo.DogInfoActivity;
import com.example.dognet.data.DogInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DogListView {

    private final String IMG_FOLDER2 = "imgs/views";
    private final String IMG_FOLDER = "imgs/icons";
    private static final String EXTRA_MESSAGE = "ClassificationImage";
    private ListView listViewDogs = null;
    private CustomDogList customDogList = null;
    private Activity activity = null;

    public ArrayList getDogBreed() {
        return dogBreed;
    }

    //
    private ArrayList dogBreed = null;
    private ArrayList dogOtherName = null;
    private ArrayList dogFci = null;
    private List<Integer> dogIds = null;
    public static List<DogInfo> dogs = null;
    private List<DogInfo> filteredDogs = null;

    public static Bitmap[] getDogIcons() {
        return dogIcons;
    }

    static private Bitmap[] dogIcons;
    private Bitmap[] dogIcons2;
    private AssetManager assetManager = null;
    private Context context = null;

    public List<DogInfo> getDogs() {
        return this.dogs;
    }

    public DogListView(Activity activity, ListView listViewDogs, List<DogInfo> dogs) {
        this.context = activity.getApplicationContext();
        this.assetManager = context.getAssets();
        this.listViewDogs = listViewDogs;
        this.activity = activity;
        this.dogs = dogs;
        this.filteredDogs = dogs;

    }
    public void resetDogList() {
        dogIcons2 = dogIcons;
        filteredDogs = dogs;
    }

    public int imgsAssetsTest1(List<DogInfo> dogs) {
        int countErrors = 0;

        for (int i = 0; i < dogs.size(); i++) {
            Bitmap img = this.getBitmapFromAsset(IMG_FOLDER + "/" + dogs.get(i).icon + ".jpg");
            if (img == null) {
                Log.println(Log.DEBUG,"DogNet", dogs.get(i).icon + ".jpg");
                countErrors++;
            }
        }
        return countErrors;
    }
    public int imgsAssetsTest2(List<DogInfo> dogs) {
        int countErrors = 0;
        for (int i = 0; i < dogs.size(); i++) {
            Bitmap img = this.getBitmapFromAsset(IMG_FOLDER2 + "/" + dogs.get(i).icon + ".jpg");
            if (img == null){
                img = this.getBitmapFromAsset(IMG_FOLDER2 + "/" + dogs.get(i).icon + ".JPG");
                if (img == null) {
                    Log.println(Log.DEBUG, "DogNet", dogs.get(i).icon + ".jpg");
                    countErrors++;
                }
            }
        }
        return countErrors;
    }
    public void refreshResultsListViewDogs(String [] results)
    {
        dogBreed = new ArrayList();
        dogOtherName = new ArrayList();
        dogFci = new ArrayList();
        for(int i=0; i < this.filteredDogs.size(); i++) {
            dogBreed.add(this.filteredDogs.get(i).breed);
            dogOtherName.add(this.filteredDogs.get(i).otherName);
            if (this.filteredDogs.get(i).fci_number != -1)
                dogFci.add("FCI: " + String.valueOf(this.filteredDogs.get(i).fci_number));
            else
                dogFci.add("No FCI");
        }
        if (this.dogIcons == null || this.dogIcons.length < dogs.size()) {
            this.dogIcons = new Bitmap[dogBreed.size()];
            for (int i = 0; i < this.filteredDogs.size(); i++) {
                Bitmap img = this.getBitmapFromAsset(IMG_FOLDER + "/" + this.filteredDogs.get(i).icon + ".jpg");
                if (img == null) {
                    img = this.getBitmapFromAsset(IMG_FOLDER + "/" + "None_icon.jpg");
                }
                this.dogIcons[i] = img;
            }
        }
        this.dogIcons2 = this.dogIcons.clone();
        if (filteredDogs.size() != this.dogIcons.length) {
            for (int i = 0; i < this.dogIds.size(); i++) {
                this.dogIcons2[i] = this.dogIcons[this.dogIds.get(i)];
            }
        }
        // else {
        //    this.dogIcons2 = this.dogIcons.clone();
        //}
        this.listViewDogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(activity.getApplicationContext(), DogInfoActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST",(Serializable)filteredDogs.get(position));
                intent.putExtra(EXTRA_MESSAGE,args);
                activity.startActivity(intent);
            }
        });
        String[] stringArray = (String[]) this.dogBreed.toArray(new String[dogBreed.size()]);
        //String[] stringArray2 = (String[]) this.dogOtherName.toArray(new String[dogOtherName.size()]);
        String[] stringArray3 = (String[]) this.dogFci.toArray(new String[dogFci.size()]);
        this.customDogList = new CustomDogList(activity, (String[]) stringArray, results, stringArray3, dogIcons2);
        this.listViewDogs.setAdapter(this.customDogList);
    }
    public void refreshListViewDogs()
    {
        dogBreed = new ArrayList();
        dogOtherName = new ArrayList();
        dogFci = new ArrayList();
        for(int i=0; i < this.filteredDogs.size(); i++) {
            dogBreed.add(this.filteredDogs.get(i).breed);
            dogOtherName.add(this.filteredDogs.get(i).otherName);
            if (this.filteredDogs.get(i).fci_number != -1)
                dogFci.add("FCI: " + String.valueOf(this.filteredDogs.get(i).fci_number));
            else
                dogFci.add("No FCI");
        }
        if (this.dogIcons == null || this.dogIcons.length < dogs.size()) {
            this.dogIcons = new Bitmap[dogBreed.size()];
            for (int i = 0; i < this.filteredDogs.size(); i++) {
                Bitmap img = this.getBitmapFromAsset(IMG_FOLDER + "/" + this.filteredDogs.get(i).icon + ".jpg");
                if (img == null) {
                    img = this.getBitmapFromAsset(IMG_FOLDER + "/" + "None_icon.jpg");
                }
                this.dogIcons[i] = img;
            }
        }
        this.dogIcons2 = this.dogIcons.clone();
        if (filteredDogs.size() != this.dogIcons.length) {
            for (int i = 0; i < this.dogIds.size(); i++) {
                this.dogIcons2[i] = this.dogIcons[this.dogIds.get(i)];
            }
        } // {
        //    this.dogIcons2 = this.dogIcons.clone();
        // }
        this.listViewDogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(activity.getApplicationContext(), DogInfoActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST",(Serializable)filteredDogs.get(position));
                intent.putExtra(EXTRA_MESSAGE,args);
                activity.startActivity(intent);
            }
        });
        String[] stringArray = (String[]) this.dogBreed.toArray(new String[dogBreed.size()]);
        String[] stringArray2 = (String[]) this.dogOtherName.toArray(new String[dogOtherName.size()]);
        String[] stringArray3 = (String[]) this.dogFci.toArray(new String[dogFci.size()]);
        this.customDogList = new CustomDogList(activity, (String[]) stringArray, stringArray2, stringArray3, dogIcons2);
        this.listViewDogs.setAdapter(this.customDogList);
    }

    public Bitmap getBitmapFromAsset(String filePath) {

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }
    public void filterDogs(int[] ids)
    {
        this.dogIds = new ArrayList();
        ArrayList<DogInfo> newFilteredDogs = new ArrayList<>();
        for(int i = 0; i < ids.length; i++) {
            newFilteredDogs.add(this.dogs.get(ids[i]));
            this.dogIds.add(ids[i]);
        }
        this.filteredDogs = newFilteredDogs;
    }
    public void filterDogs(String text, int[] filters)
    {
        int sum = 0;
        for(int i=0;i<filters.length;i++)
            sum += filters[i];
        if (text.length() > 0 && sum > 0) {
            this.dogIds = new ArrayList();
            ArrayList<DogInfo> newFilteredDogs = new ArrayList<>();
            for (int i = 0; i < this.dogs.size(); i++) {
                String text1 = text.toLowerCase();
                String text2 = this.dogs.get(i).breed.toLowerCase();
                String text3 = String.valueOf(this.dogs.get(i).fci_number);
                String text4 = this.dogs.get(i).otherName.toLowerCase();

                boolean result1 = filters[0] == 1 ? text2.matches(".*" + text1 + ".*") : false;
                boolean result2 = filters[1] == 1 ? text3.matches(text1) : false;
                boolean result3 = filters[2] == 1 ? text4.matches(".*" + text1 + ".*") : false;
                if (result1 || result2 || result3) {
                    newFilteredDogs.add(this.dogs.get(i));
                    this.dogIds.add(i);
                }
            }
            this.filteredDogs = newFilteredDogs;
        }
    }
}
